import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UsersService } from './users.service';

export class FirstNameValidator {
  static createValidator(userService: UsersService): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors> => {
      return userService
        .checkIfFirstNameExists(control.value)
        .pipe(
          map((result: boolean) =>
            result ? { "alreadyExists": true } : {}
          )
        );
    };
  }
}