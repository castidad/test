import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { createMask } from '@ngneat/input-mask';
import { UsersService } from '../users.service';
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';
import { PhoneValidator } from '../phones.validator';
import { FirstNameValidator } from '../firstname.validator';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  public addForm: FormGroup;
  public submitted = false;
  public phoneInputMask = createMask('+38(099) 99-99-999');
  public staticAlertClosed = true;
  
  @ViewChild('staticAlert', { static: false })
  staticAlert!: NgbAlert;

  public constructor(private fb: FormBuilder, private userService: UsersService) {
    this.addForm = this.fb.group({
      first_name: [
        '',  
        { 
          validators: [Validators.minLength(6), Validators.required], 
          asyncValidators: [FirstNameValidator.createValidator(this.userService)],
          updateOn: 'blur' 
        }
      ],
      last_name: [
        '', 
        { 
          validators: [Validators.minLength(6), Validators.required],
          updateOn: 'blur'
        }
      ],
      phones: this.fb.array([
        this.fb.control('', [Validators.required], [PhoneValidator.createValidator(this.userService)])
      ])
    });
  }

  public ngOnInit(): void {}

  public get _firstname() {
    return this.addForm.get('first_name')
  }

  public get _lastname() {
    return this.addForm.get('last_name')
  }

  public get _phones() {
    return this.addForm.get('phones') as FormArray;
  }

  public addPhone() {
    this._phones.push(this.fb.control('', [Validators.required]));
  }

  public deletePhone(index: number) {
    if (index !== -1) {
      this._phones.removeAt(index);
    }
  }

  public onSubmit() {
    this.submitted = true;
    if (this.addForm.valid) {
      this.staticAlertClosed = false;
      this.userService.addData(this.addForm.value);
    }
  }
}
