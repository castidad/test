import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

interface UsersInterface {
  first_name: string;
  last_name: string;
  phones: Array<string>;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  public users: UsersInterface[] = [];

  public constructor(private userService: UsersService){}

  public ngOnInit(): void {
    this.users = this.userService.getData();
  }

  public deleteRow(index: number) {
    if (index !== -1) {
      this.users.splice(index, 1);
    }
  }
}