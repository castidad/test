import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

interface UsersInterface {
  first_name: string;
  last_name: string;
  phones: Array<string>;
}

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private data: UsersInterface[] = [];
  private dataUrl: string = '/assets/users.json';
  
  private loadData(): Observable<UsersInterface[]> {
    return this.http.get<UsersInterface[]>(this.dataUrl);
  }

  public getData(): UsersInterface[] {
    return this.data;
  }
  
  public addData(user: UsersInterface) {      
    this.data.push(user);
  }

  public checkIfFirstNameExists(value: string) {
    return of(this.data.some((a) => a.first_name === value)).pipe(
      delay(1000)
    );
  }

  public checkIfPhoneExists(value: string) {
    return of(this.data.some((a) => a.phones.includes(value))).pipe(
      delay(1000)
    );
  }

  public constructor(private http: HttpClient) { 
    this.loadData()
      .subscribe((repsonse: UsersInterface[]) => {
        repsonse.map((el)=> this.addData(el));
      });
  }
}
